CROSS_COMPILE=arm-none-eabi-
AS=$(CROSS_COMPILE)as
CC=$(CROSS_COMPILE)gcc
LD=$(CROSS_COMPILE)ld

AFLAGS=
CFLAGS=-c -O2 -std=gnu2x -ffreestanding -ffunction-sections -fdata-sections -Wall -Wextra -std=gnu2x \
	   -march=armv7 -Iinclude -g
LFLAGS=-gc-sections

CSRC=$(wildcard src/*.c)
ASRC=$(wildcard src/*.s)
SRC=$(CSRC) $(ASRC)
OBJ=$(SRC:src/%=build/obj/%.o)
DEPS=$(CSRC:src/%.c=build/deps/%.deps)

install/boot/aleph: $(OBJ) map.ld
	@mkdir -p $(@D)
	$(LD) $(LFLAGS) -o $@ -T map.ld $(OBJ) $(shell $(CC) -print-libgcc-file-name)

build/deps/%.deps: src/%.c Makefile
	@mkdir -p $(@D)
	$(CC) $(CFLAGS) -M $< | sed 's/\(.*\)\.o:/build\/obj\/\1.c.o:/' > $@

include $(DEPS)

build/obj/%.c.o: src/%.c Makefile
	@mkdir -p $(@D)
	$(CC) $(CFLAGS) -o $@ $<

build/obj/%.s.o: src/%.s Makefile
	@mkdir -p $(@D)
	$(AS) $(AFLAGS) -o $@ $<

clean:
	rm -r build install 2> /dev/null || true

.PHONY: clean
