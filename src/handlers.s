.syntax unified

.section .text.asm_start
.global asm_start
asm_start:
	cps 0b11011	// undefined
	ldr sp, =#-1024 * 1024 - 256 * 1024 * 2

	cps 0b11111	// system
	ldr sp, =#0
	ldr r1, =vectors
	mcr p15, 0, r1, c12, c0, 0
	b start

/*
.section .text.asm_supervisor_call
.global asm_supervisor_call
asm_supervisor_call:
	ldr sp, =#-1024 * 1024
	push {r0-r3, lr}

	bl process_current_ptr
	add r1, r0, #16
	stm r1, {r4-r12}

	pop {r4-r7, lr}
	stm r0, {r4-r7}

	str lr, [r0, #15 * 16]
	cps 0b11111	// system mode
	str sp, [r0, #13 * 16]
	str lr, [r0, #14 * 16]
	cps 0b10011	// supervisor mode

	bl process_stop_current

	mov r0, r4
	mov r1, r5
	mov r2, r6
	mov r3, r7
	bl syscall_schedule
	b process_schedule_runner
*/

.section .text.asm_data_abort
.global asm_data_abort
asm_data_abort:
	ldr sp, =#-1024 * 1024 - 256 * 1024

	push {r0-r3, lr}
	mrs r1, spsr
	and r1, 0b11111
	cmp r1, 0b11111
	bne panic

	bl mem_check_delayed_map_on_abort
	cmp r0, 0

	// retry
	pop {r0-r3, lr}
	subsne pc, lr, #12
	//b panic
	b 0x402f0410

.section .text.asm_on_irq
.global asm_on_irq
asm_on_irq:
	ldr sp, =#-1024 * 1024 - 256 * 1024 * 3
	push {r0-r3, lr}
	bl intc_on_irq
	pop {r0-r3, lr}
	subs pc, lr, #4
