#pragma once
#include <stddef.h>
#include <aleph.h>
#include <stdint.h>

typedef void (*ObjectCallNextFunc)(void *arg, ssize_t call_result);

typedef struct ObjectVTable
{
	void (*cleanup)(void *state);
	ssize_t (*write)(void *state, const char *buffer, size_t length);
	ssize_t (*read)(void *state, char *buffer, size_t length);
	ssize_t (*set_pos)(void *state, off64_t position);
	/*
	size_t num_custom_calls;
	ssize_t (**custom_calls)(void *state, size_t caller_pid, size_t a, size_t b, size_t c);
	*/
} ObjectVTable;

ssize_t object_register(const ObjectVTable *vtable, void *state);
ssize_t object_destroy(ssize_t id);
ssize_t object_write(ssize_t obj, const char *buffer, size_t length);
ssize_t object_write_cstr(ssize_t obj, const char *str);
ssize_t object_write_u32(ssize_t obj, uint32_t value);
ssize_t object_read(ssize_t obj, char *buffer, size_t length);
ssize_t object_set_pos(ssize_t obj, off64_t position);
