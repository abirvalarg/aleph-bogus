#pragma once
#include <stddef.h>

void heap_init(size_t size);
void *heap_alloc(size_t size);
void heap_free(void *ptr);
