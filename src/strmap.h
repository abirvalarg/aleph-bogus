#pragma once
#include "str.h"
#include <stddef.h>

typedef struct StrMapFrame StrMapFrame;

typedef struct StrMap
{
	StrMapFrame **buckets;
	size_t num_buckets;
	size_t num_items;
} StrMap;

typedef enum StrMapStatus
{
	STRMAP_OK,
	STRMAP_OUT_OF_MEMORY,
	STRMAP_END_OF_ITER
} StrMapStatus;

typedef struct StrMapIterItem
{
	void *item;
	Str key;
	StrMapStatus status;
} StrMapIterItem;

typedef struct StrMapIter
{
	const StrMap *m;
	const StrMapFrame *current;
	size_t bucket;
} StrMapIter;

StrMap strmap_empty(void);
void strmap_del(StrMap *m);
StrMapStatus strmap_insert(StrMap *m, Str key, void *value);
void *strmap_get(const StrMap *m, Str key);
// @returns the item that's associated with the deleted frame
void *strmap_del_item(StrMap *m, Str key);
StrMapIter strmap_iter(const StrMap *m);
StrMapIterItem strmap_iter_next(StrMapIter *iter);
