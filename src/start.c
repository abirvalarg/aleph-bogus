#include <stdint.h>
#include "device.h"
#include "driver.h"
#include "heap.h"
#include "mem.h"
#include "prcm.h"
#include "process.h"
#include "strmap.h"
#include "object.h"
#include "intc.h"

typedef struct AlephBootInfo
{
	const char *devlist;    // Pointer to the devlist string in bootloader memory space
	uint32_t devlist_length;    // Length of the devlist in bytes
	const char *config;         // Pointer to the config string in bootloader memory space
	uint32_t config_length;     // Length of the config in bytes
	uint32_t phys_memory_size;  // Size of physical memory in KiBs
} AlephBootInfo;

__attribute__((noreturn))
void halt(void);
static void create_devices(const char *devlist, size_t  devlist_length);

__attribute__((noreturn))
void start(const AlephBootInfo *boot_info)
{
	mem_init(boot_info->phys_memory_size);
	heap_init(256 * 1024 * 1024);

	driver_init();
	device_init();
	if(prcm_init() != OK)
		halt();
	if(intc_init() != OK)
		halt();
	if(process_init() != OK)
		halt();

	asm("cpsie i");

	create_devices(boot_info->devlist, boot_info->devlist_length);

	ssize_t uart = device_open(str_from_cstr("uart0"), OPEN_READ | OPEN_WRITE);
	if(uart < 0)
		halt();

	object_write_cstr(uart, "\r\nHello\r\n");
	object_write_cstr(uart, "memory usage: ");
	object_write_u32(uart, mem_usage());
	object_write_cstr(uart, " KiB\r\n");
	while(1)
	{
		static char buffer[4096];
		ssize_t len = object_read(uart, buffer, 4096);
		if(len > 0)
		{
			ssize_t sent_total = 0;
			while(sent_total < len)
			{
				ssize_t sent = object_write(uart, buffer, len);
				if(sent < 0)
					break;
				sent_total += sent;
			}
		}
	}
	halt();
}

static void create_devices(const char *devlist, size_t devlist_length)
{
	size_t devname_start = 0, devname_end = 0, driver_start = 0, driver_end = 0,
		option_start = 0, option_end = 0, value_start = 0;
	StrMap options = strmap_empty();
	char in_trailing = 1, device_decl = 0, driver_name = 0, in_option = 0,
		in_comment = 0, in_value = 0;
	for(size_t pos = 0; pos < devlist_length; pos++)
	{
		char ch = devlist[pos];
		if(!in_comment)
		{
			if(ch == '#' && in_trailing)
				in_comment = 1;
			else if(ch == '=' && in_option)
			{
				in_value = 1;
				option_end = pos;
				in_trailing = 1;
			}
			else if(ch == '!' && in_trailing)
			{
				if(devname_end > devname_start)
				{
					Str devname = {
						.contents = devlist + devname_start,
						.length = devname_end - devname_start
					};
					Str driver = {
						.contents = devlist + driver_start,
						.length = driver_end - driver_start
					};
					driver_create_device(driver, devname, &options);

					// cleanup the options
					StrMapIter iter = strmap_iter(&options);
					while(1)
					{
						StrMapIterItem next = strmap_iter_next(&iter);
						if(next.status == STRMAP_END_OF_ITER)
							break;
						heap_free(next.item);
					}

					strmap_del(&options);
					options = strmap_empty();
				}
				device_decl = 1;
			}
			else if(ch == ':' && device_decl)
			{
				driver_name = 1;
				in_trailing = 1;
				devname_end = pos;
			}
			else if(ch > ' ')
			{
				if(in_trailing)
				{
					if(device_decl && !driver_name)
						devname_start = pos;
					else if(driver_name)
						driver_start = pos;
					else if(in_value)
						value_start = pos;
					else
					{
						option_start = pos;
						in_option = 1;
					}
					in_trailing = 0;
				}
			}
		}
		if(ch == '\n' || ch == '\r')
		{
			if(driver_name)
				driver_end = pos;
			else if(in_value)
			{
				Str *option_ptr = heap_alloc(sizeof(Str));
				if(!option_ptr)
					halt();
				*option_ptr = (Str) {
					.contents = devlist + value_start,
					.length = pos - value_start
				};
				Str name = {
					.contents = devlist + option_start,
					.length = option_end - option_start
				};
				StrMapStatus st = strmap_insert(&options, name, option_ptr);
				if(st == STRMAP_OUT_OF_MEMORY)
					halt();
			}
			in_trailing = 1;
			device_decl = 0;
			driver_name = 0;
			in_option = 0;
			in_comment = 0;
			in_value = 0;
		}
	}
	if(devname_end > devname_start)
	{
		Str devname = {
			.contents = devlist + devname_start,
			.length = devname_end - devname_start
		};
		Str driver = {
			.contents = devlist + driver_start,
			.length = driver_end - driver_start
		};
		driver_create_device(driver, devname, &options);

		// cleanup the options
		StrMapIter iter = strmap_iter(&options);
		while(1)
		{
			StrMapIterItem next = strmap_iter_next(&iter);
			if(next.status == STRMAP_END_OF_ITER)
				break;
			heap_free(next.item);
		}
	}
	strmap_del(&options);
}

void halt(void)
{
	while(1);
}
