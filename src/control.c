#include "control.h"
#include "mem.h"
#include <aleph.h>

static volatile size_t *regs = 0;

ssize_t control_init(void)
{
	regs = mem_map_device(0x44e1000, 128 * 1024);
	return regs ? OK : ERR_OUT_OF_MEMORY;
}

void control_pin_mode(size_t offset, size_t options)
{
	regs[offset / 4] = options;
}
