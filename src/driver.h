#pragma once
#include <aleph.h>
#include "str.h"
#include "strmap.h"

typedef struct Driver
{
	const char *name;
	void (*init)(void);
	void (*cleanup)(void);
	ssize_t (*create_device)(Str name, const StrMap *options);
} Driver;

void driver_init(void);
void driver_cleanup(void);
// @param options is a StrMap pointer with pointers to `Str` as items
ssize_t driver_create_device(Str driver, Str name, const StrMap *options);
int driver_exists(Str name);
