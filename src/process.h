#pragma once
#include <aleph.h>

ssize_t process_init(void);
void process_pause(void);
ssize_t process_resume(ThreadID id);
ThreadID process_current_thread(void);
