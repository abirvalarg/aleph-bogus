#include "mutex.h"
#include <aleph.h>
#include "process.h"

typedef struct MutexWaitListNode
{
	struct MutexWaitListNode *next;
	ThreadID thread;
} MutexWaitListNode;

size_t mutex_internal_try_lock(_Atomic size_t *m);

void mutex_lock(Mutex *m)
{
	size_t was_locked = mutex_internal_try_lock(&m->locked);
	if(was_locked)
	{
		MutexWaitListNode node = {
			.thread = process_current_thread()
		};

		while(mutex_internal_try_lock(&m->queue_lock));
		if(m->wait_list_tail)
			m->wait_list_tail->next = &node;
		else
			m->wait_list = m->wait_list_tail = &node;
		m->queue_lock = 0;

		do
		{
			process_pause();
		} while(m->wait_list != &node);

		while(mutex_internal_try_lock(&m->queue_lock));
		m->wait_list = node.next;
		if(node.next == 0)
			m->wait_list_tail = 0;
		m->queue_lock = 0;
	}
}

void mutex_unlock(Mutex *m)
{
	while(mutex_internal_try_lock(&m->queue_lock));
	if(m->wait_list)
		process_resume(m->wait_list->thread);
	else
		m->locked = 0;
	m->queue_lock = 0;
}
