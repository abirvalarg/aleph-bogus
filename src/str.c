#include "str.h"

Str str_from_cstr(const char *src)
{
	size_t length = 0;
	while(src[length])
		length++;
	if(length)
	{
		return (Str) {
			.contents = src,
			.length = length
		};
	}
	else
		return (Str) { 0 };
}

int str_equal(Str a, Str b)
{
	if(a.length == b.length)
	{
		for(size_t pos = 0; pos < a.length; pos++)
			if(a.contents[pos] != b.contents[pos])
				return 0;
		return 1;
	}
	return 0;
}

StrToU32Result str_hex_to_u32(Str src)
{
	uint32_t result = 0;
	for(size_t pos = 0; pos < src.length; pos++)
	{
		char ch = src.contents[pos];
		if(('0' <= ch && ch <= '9') || ('A' <= ch && ch <= 'F') || ('a' <= ch && ch <= 'f'))
			result = result << 4 | (ch > '9' ? (ch | 0x20) - 'a' + 10 : ch - '0');
		else if(ch != '_')
			return (StrToU32Result) { .success = 0 };
	}
	return (StrToU32Result) {
		.result = result,
		.success = 1
	};
}

StrToU32Result str_bin_to_u32(Str src)
{
	uint32_t result = 0;
	for(size_t pos = 0; pos < src.length; pos++)
	{
		char ch = src.contents[pos];
		if(('0' <= ch && ch <= '1'))
			result = result << 4 | (ch - '0');
		else if(ch != '_')
			return (StrToU32Result) { .success = 0 };
	}
	return (StrToU32Result) {
		.result = result,
		.success = 1
	};
}
