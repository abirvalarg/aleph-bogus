#include "prcm.h"
#include <aleph.h>
#include "mem.h"
#include <stddef.h>

static volatile size_t *regs = 0;
static const size_t submodule_offsets[] = {
	[PRCM_CM_PER] = 0,
	[PRCM_CM_WKUP] = 1024 / 4
};

ssize_t prcm_init(void)
{
	regs = mem_map_device(0x44e00000, 4096);
	return regs ? OK : ERR_OUT_OF_MEMORY;
}

void prcm_switch(PrcmSubmodule submodule, size_t position, PrcmState state)
{
	size_t suboff = submodule_offsets[submodule];
	size_t reg = regs[suboff + position / 4];
	reg &= 0b11;
	reg |= state;
	regs[suboff + position / 4] = reg;
}
