#include "strmap.h"
#include "strbuf.h"
#include "heap.h"

struct StrMapFrame
{
	StrMapFrame *next;
	void *data;
	size_t hash;
	StrBuf key;
};

static size_t calc_hash(Str key);

StrMap strmap_empty(void)
{
	return (StrMap) {
		.buckets = 0,
		.num_buckets = 0,
		.num_items = 0
	};
}

void strmap_del(StrMap *m)
{
	for(size_t bucket = 0; bucket < m->num_buckets; bucket++)
	{
		StrMapFrame *frame = m->buckets[bucket];
		while(frame)
		{
			StrMapFrame *next = frame->next;
			strbuf_del(&frame->key);
			heap_free(frame);
			frame = next;
		}
	}
	if(m->buckets)
		heap_free(m->buckets);
}

StrMapStatus strmap_insert(StrMap *m, Str key, void *value)
{
	if(m->num_buckets == m->num_items)
	{
		// create a new buckets array
		size_t new_num_buckets = m->num_buckets ? m->num_buckets * 2 : 1;
		if(new_num_buckets <= m->num_buckets)
			return STRMAP_OUT_OF_MEMORY;
		StrMapFrame **new_buckets = heap_alloc(sizeof(StrMapFrame*) * new_num_buckets);
		if(!new_buckets)
			return STRMAP_OUT_OF_MEMORY;

		for(size_t i = 0; i < new_num_buckets; i++)
			new_buckets[i] = 0;

		// create chain cache
		StrMapFrame ***next_locations_cache = heap_alloc(sizeof(void*) * new_num_buckets);
		if(!next_locations_cache)
		{
			heap_free(new_buckets);
			return STRMAP_OUT_OF_MEMORY;
		}
		for(size_t i = 0; i < new_num_buckets; i++)
			next_locations_cache[i] = &new_buckets[i];

		// populate new buckets;
		for(size_t i = 0; i < m->num_buckets; i++)
		{
			StrMapFrame *frame = m->buckets[i];
			while(frame)
			{
				StrMapFrame *next = frame->next;
				frame->next = 0;
				size_t target_bucket = frame->hash & (new_num_buckets - 1);
				*(next_locations_cache[target_bucket]) = frame;
				next_locations_cache[target_bucket] = &frame->next;
				frame = next;
			}
		}

		// cleanup and apply changes
		heap_free(next_locations_cache);
		heap_free(m->buckets);
		m->buckets = new_buckets;
		m->num_buckets = new_num_buckets;
	}

	size_t hash = calc_hash(key);
	size_t target_bucket = hash & (m->num_buckets - 1);

	StrMapFrame **location = &m->buckets[target_bucket];
	{
		StrMapFrame *head = m->buckets[target_bucket];
		while(head)
		{
			if(head->hash == hash && str_equal(strbuf_to_str(&head->key), key))
			{
				head->data = value;
				return STRMAP_OK;
			}
			location = &head->next;
			head = head->next;
		}
	}

	StrMapFrame *new_frame = heap_alloc(sizeof(StrMapFrame));
	if(!new_frame)
		return STRMAP_OUT_OF_MEMORY;
	StrBufResult strbuf_key_res = strbuf_from_str(key);
	if(strbuf_key_res.status == STRBUF_OUT_OF_MEMORY)
	{
		heap_free(new_frame);
		return STRMAP_OUT_OF_MEMORY;
	}
	*new_frame = (StrMapFrame) {
		.next = 0,
		.data = value,
		.hash = hash,
		.key = strbuf_key_res.result
	};
	*location = new_frame;
	m->num_items++;
	return STRMAP_OK;
}

void *strmap_get(const StrMap *m, Str key)
{
	if(!m->num_items)
		return 0;
	size_t hash = calc_hash(key);
	StrMapFrame *frame = m->buckets[hash & (m->num_buckets - 1)];
	while(frame)
	{
		if(frame->hash == hash && str_equal(strbuf_to_str(&frame->key), key))
			return frame->data;
		frame = frame->next;
	}
	return 0;
}

void *strmap_del_item(StrMap *m, Str key)
{
	size_t hash = calc_hash(key);
	size_t bucket_idx = hash % m->num_buckets;
	StrMapFrame *head = m->buckets[bucket_idx];
	StrMapFrame **next_location = &m->buckets[bucket_idx];
	while(head)
	{
		StrMapFrame *next = head->next;
		if(head->hash == hash && str_equal(strbuf_to_str(&head->key), key))
		{
			void *ret = head->data;
			*next_location = head->next;
			strbuf_del(&head->key);
			heap_free(head);
			return ret;
		}
		else
			next_location = &head->next;
		head = next;
	}
	return 0;
}

StrMapIter strmap_iter(const StrMap *m)
{
	for(size_t bucket = 0; bucket < m->num_buckets; bucket++)
	{
		if(m->buckets[bucket])
		{
			return (StrMapIter) {
				.m = m,
				.current = m->buckets[bucket],
				.bucket = bucket
			};
		}
	}
	return (StrMapIter) {
		.m = m,
		.current = 0,
		.bucket = m->num_buckets - 1
	};
}

StrMapIterItem strmap_iter_next(StrMapIter *iter)
{
	StrMapFrame *next = iter->current->next;
	if(next)
	{
		iter->current = next;
		return (StrMapIterItem) {
			.status = STRMAP_OK,
			.item = next->data,
			.key = strbuf_to_str(&next->key)
		};
	}
	else
	{
		for(size_t b = iter->bucket + 1; b < iter->m->num_buckets; b++)
		{
			if(iter->m->buckets[b])
			{
				iter->bucket = b;
				iter->current = iter->m->buckets[b];
				return (StrMapIterItem) {
					.status = STRMAP_OK,
					.item = iter->current->data,
					.key = strbuf_to_str(&iter->current->key)
				};
			}
		}
		iter->current = 0;
		iter->bucket = iter->m->num_buckets - 1;
		return (StrMapIterItem) {
			.status = STRMAP_END_OF_ITER
		};
	}
}

static size_t calc_hash(Str key)
{
	size_t hash = 5381;
	for(size_t i = 0; i < key.length; i++)
		hash = ((hash << 5) + hash) ^ key.contents[i];
	return hash;
}
