#include "process.h"
#include <aleph.h>
#include "heap.h"

#define INITIAL_NUM_PROCESSES 16

typedef struct ObjectBox
{
	ssize_t object;
	size_t rc;
} ObjectBox;

typedef struct Thread
{
	size_t regs[16];
	size_t psr;
	enum
	{
		RUNNING,
		SCHEDULED,
		PAUSED,
		DEAD
	} state;
} Thread;

typedef struct Process
{
	Thread *threads;
	size_t num_threads;
	ObjectBox **objects;
	size_t num_objects;
	uint8_t valid: 1;
} Process;

static size_t current_process = 0;
static size_t current_thread = 0;
static Process *processes = 0;
static size_t num_processes = 0;

ssize_t process_init(void)
{
	processes = heap_alloc(sizeof(Process) * INITIAL_NUM_PROCESSES);
	if(!processes)
		return ERR_OUT_OF_MEMORY;
	num_processes = INITIAL_NUM_PROCESSES;
	for(size_t i = 1; i < INITIAL_NUM_PROCESSES; i++)
		processes[i].valid = 0;

	Thread *threads = heap_alloc(sizeof(Thread));
	if(!threads)
	{
		processes[0].valid = 0;
		return ERR_OUT_OF_MEMORY;
	}
	threads[0] = (Thread) {
		.state = RUNNING
	};

	processes[0] = (Process) {
		.threads = threads,
		.num_threads = 1,
		.valid = 1
	};
	return OK;
}

void process_pause(void)
{
	Thread *t = &processes[current_process].threads[current_thread];
	t->state = PAUSED;
}

ssize_t process_resume(ThreadID id)
{
	if(id.process < 0 || id.thread < 0)
		return ERR_NONE;
	Process *p = &processes[id.process];
	if(!p->valid)
		return ERR_NONE;
	if((size_t)id.thread >= p->num_threads)
		return ERR_NONE;
	switch(p->threads[id.thread].state)
	{
	case RUNNING:
	case SCHEDULED:
		return ERR_BUSY;

	case PAUSED:
		p->threads[id.thread].state = SCHEDULED;
		return OK;

	case DEAD:
		return ERR_NONE;
	}
	return ERR_NOT_IMPLEMENTED;
}

ThreadID process_current_thread(void)
{
	return (ThreadID) {
		.process = current_process,
		.thread = current_thread
	};
}
