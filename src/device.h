#pragma once
#include <aleph.h>
#include <stddef.h>
#include "str.h"
#include "strmap.h"

typedef struct DeviceVTable
{
	void (*cleanup)(void *state);
	// @returns the object id on success
	ssize_t (*open)(void *state, size_t flags);
	ssize_t (*create_subdevice)(void *state, Str child_name, const StrMap *options);
} DeviceVTable;

void device_init(void);
void device_cleanup(void);
ssize_t device_register(Str name, const DeviceVTable *vtable, void *state);
ssize_t device_remove(Str name);
ssize_t device_open(Str name, size_t flags);
ssize_t device_create_subdevice(Str parent_name, Str child_name, const StrMap *options);
