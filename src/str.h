#pragma once
#include <stddef.h>
#include <stdint.h>

typedef struct Str
{
	const char *contents;
	size_t length;
} Str;

typedef struct StrToU32Result
{
	uint32_t result;
	char success;
} StrToU32Result;

#define STR_EMPTY ((Str) { 0 })

Str str_from_cstr(const char *src);
int str_equal(Str a, Str b);
StrToU32Result str_hex_to_u32(Str src);
StrToU32Result str_bin_to_u32(Str src);
