#pragma once
#include <stddef.h>
#include <aleph.h>

// typedef void (*IntcHandler)(size_t int_num);
typedef struct IntcHandler
{
	void (*handler)(size_t int_num, void *arg);
	void *arg;
} IntcHandler;

ssize_t intc_init(void);
/* Set the interrupt handler and unmask the interrupt
 *
 * @returns the old handler
 */
IntcHandler intc_set_handler(size_t int_num, IntcHandler new_handler);
void intc_on_irq(void);
