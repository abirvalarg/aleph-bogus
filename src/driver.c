#include "driver.h"
#include "uart.h"
// #include "i2c.h"

static const Driver *drivers[] = {
	&uart_driver,
	// &i2c_driver
};

#define NUM_DRIVERS (sizeof(drivers) / sizeof(drivers[0]))

void driver_init(void)
{
	for(size_t i = 0; i < NUM_DRIVERS; i++)
		if(drivers[i]->init)
			drivers[i]->init();
}

void driver_cleanup(void)
{
	size_t num_drivers = sizeof(drivers) / sizeof(drivers[0]);
	for(size_t i = 0; i < num_drivers; i++)
		if(drivers[i]->cleanup)
			drivers[i]->cleanup();
}

ssize_t driver_create_device(Str driver, Str name, const StrMap *options)
{
	for(size_t i = 0; i < NUM_DRIVERS; i++)
	{
		if(str_equal(str_from_cstr(drivers[i]->name), driver))
		{
			if(drivers[i]->create_device)
				return drivers[i]->create_device(name, options);
			else
				return ERR_NO_CALL;
		}
	}
	return ERR_NONE;
}

int driver_exists(Str name)
{
	for(size_t i = 0; i < NUM_DRIVERS; i++)
		if(str_equal(str_from_cstr(drivers[i]->name), name))
			return 1;
	return 0;
}
