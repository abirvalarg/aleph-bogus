#pragma once
#include <aleph.h>
#include <stdint.h>

typedef struct Mutex
{
	struct MutexWaitListNode *wait_list;
	struct MutexWaitListNode *wait_list_tail;
	_Atomic size_t locked;		// used in `mutex.s`
	_Atomic size_t queue_lock;	// used in `mutex.s`
} Mutex;

#define MUTEX_NEW (Mutex) { 0 }

void mutex_lock(Mutex *m);
void mutex_unlock(Mutex *m);
