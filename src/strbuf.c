#include "strbuf.h"
#include "heap.h"
#include "mem.h"

StrBuf StrBuf_new(void)
{
	return (StrBuf) {
		.contents = 0,
		.length = 0,
		.capacity = 0
	};
}

StrBufResult strbuf_with_capacity(size_t capacity)
{
	if(capacity)
	{
		char *contents = heap_alloc(capacity);
		if(!contents)
			return (StrBufResult) { .status = STRBUF_OUT_OF_MEMORY };
		return (StrBufResult) {
			.status = STRBUF_OK,
			.result = {
				.contents = contents,
				.length = 0,
				.capacity = capacity
			}
		};
	}
	else
	{
		return (StrBufResult) {
			.status = STRBUF_OK,
			.result = { 0 }
		};
	}
}

StrBufResult strbuf_from_str(Str src)
{
	char *contents = heap_alloc(src.length);
	if(!contents)
		return (StrBufResult) { .status = STRBUF_OUT_OF_MEMORY };
	memcpy(contents, src.contents, src.length);
	return (StrBufResult) {
		.status = STRBUF_OK,
		.result = {
			.contents = contents,
			.length = src.length,
			.capacity = src.length
		}
	};
}

void strbuf_del(StrBuf *str)
{
	if(str->contents)
		heap_free(str->contents);
}

StrBufStatus strbuf_push(StrBuf *string, char ch)
{
	if(string->length == string->capacity)
	{
		size_t new_capacity = (string->capacity << 1) + 1;
		if(new_capacity <= string->capacity)
			return STRBUF_OUT_OF_MEMORY;
		char *new_contents = heap_alloc(new_capacity);
		if(!new_contents)
			return STRBUF_OUT_OF_MEMORY;
		memcpy(new_contents, string->contents, string->length);
		heap_free(string->contents);
		string->contents = new_contents;
		string->capacity = new_capacity;
	}
	string->contents[string->length++] = ch;
	return STRBUF_OK;
}

StrBufStatus strbuf_append(StrBuf *string, Str other)
{
	size_t new_length = string->length + other.length;
	if(new_length > string->capacity)
	{
		char *new_contents = heap_alloc(new_length);
		if(!new_contents)
			return STRBUF_OUT_OF_MEMORY;
		memcpy(new_contents, string->contents, string->length);
		heap_free(string->contents);
		string->contents = new_contents;
		string->capacity = new_length;
	}
	memcpy(string->contents + string->length, other.contents, other.length);
	string->length = new_length;
	return STRBUF_OK;
}

Str strbuf_to_str(const StrBuf *buf)
{
	return (Str) {
		.contents = buf->contents,
		.length = buf->length
	};
}
