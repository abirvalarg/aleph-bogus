#pragma once
#include <stddef.h>

typedef enum MemAllocationBlocks
{
	MEM_ALLOC_SMALL_PAGES,
	MEM_ALLOC_LARGE_PAGES,
	MEM_ALLOC_SECTIONS,
	MEM_ALLOC_SUPERSECTIONS
} MemAllocationBlocks;

void mem_init(size_t total_mem_kib);
// allocate a piece of memory in physical memory space
void *mem_alloc(size_t size, size_t alignment);
void mem_free(void *space, size_t size);
/* Allocate `size` bytes of virtual memory in virtual space but
 * do not assign physical RAM to it before it's needed
 */
void *mem_valloc_delayed(size_t size, MemAllocationBlocks blocks);
void *mem_map_device(size_t phys_addr, size_t size);
void mem_unmap(void *virt_addr, size_t size);

void mem_map_process_memmap(volatile size_t *map);

void *memcpy(void *restrict dest, const void *restrict src, size_t count);
void *memset(void *dest, int ch, size_t count);

size_t mem_usage(void);

/* Check if abort is caused by access to delayed memory region and map
 * memory to that area.
 *
 * @returns 1 if the access shouldn't cause an error anymore
 */
int mem_check_delayed_map_on_abort(void);
