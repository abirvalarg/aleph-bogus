.syntax unified

.section .text.mutex_internal_try_lock
.thumb
.global mutex_internal_try_lock
mutex_internal_try_lock:
	ldr r1, =#1
0:
	ldrex r2, [r0]
	strex r3, r1, [r0]
	cmp r3, #1
	beq 0b
	mov r0, r2
	bx lr
