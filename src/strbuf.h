#pragma once
#include <stddef.h>
#include "str.h"

typedef struct StrBuf
{
	char *contents;
	size_t length;
	size_t capacity;
} StrBuf;

typedef enum StrBufStatus
{
	STRBUF_OK,
	STRBUF_OUT_OF_MEMORY
} StrBufStatus;

typedef struct StrBufResult
{
	StrBuf result;
	StrBufStatus status;
} StrBufResult;

StrBuf strbuf_new(void);
StrBufResult strbuf_with_capacity(size_t capacity);
StrBufResult strbuf_from_str(Str src);
void strbuf_del(StrBuf *str);
StrBufStatus strbuf_push(StrBuf *str, char ch);
StrBufStatus strbuf_append(StrBuf *str, Str part);
Str strbuf_to_str(const StrBuf *buf);
