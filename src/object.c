#include "object.h"
#include <aleph.h>
#include "heap.h"
#include "mem.h"

typedef struct Object
{
	void *state;
	const ObjectVTable *vtable;
	unsigned char valid;
} Object;

static Object *objects = 0;
static ssize_t capacity = 0;
static ssize_t first_invalid = 0;

ssize_t object_register(const ObjectVTable *vtable, void *state)
{
	if(first_invalid == capacity)
	{
		size_t new_capacity = capacity * 2 + 1;
		Object *new_array = heap_alloc(sizeof(Object) * new_capacity);
		if(!new_array)
			return ERR_OUT_OF_MEMORY;
		memcpy(new_array, objects, sizeof(Object) * capacity);
		heap_free(objects);
		for(size_t i = capacity; i < new_capacity; i++)
			new_array[i].valid = 0;
		objects = new_array;
		capacity = new_capacity;
	}
	objects[first_invalid] = (Object) {
		.state = state,
		.vtable = vtable,
		.valid = 1
	};
	while(first_invalid < capacity && objects[first_invalid].valid)
		first_invalid++;
	return OK;
}

ssize_t object_destroy(ssize_t id)
{
	if(id < capacity && objects[id].valid)
	{
		if(objects[id].vtable->cleanup)
			objects[id].vtable->cleanup(objects[id].state);
		objects[id].valid = 0;
		if(id < first_invalid)
			first_invalid = id;
		return OK;
	}
	else
		return ERR_NONE;
}

ssize_t object_write(ssize_t obj, const char *buffer, size_t length)
{
	if(obj < capacity && objects[obj].valid)
	{
		if(objects[obj].vtable->write)
			return objects[obj].vtable->write(objects[obj].state, buffer, length);
		else
			return ERR_NO_CALL;
	}
	else
		return ERR_NONE;
}

ssize_t object_write_cstr(ssize_t obj, const char *str)
{
	size_t length = 0;
	while(str[length])
		length++;
	return object_write(obj, str, length);
}

ssize_t object_write_u32(ssize_t obj, uint32_t value)
{
	if(value == 0)
		return object_write(obj, "0", 1);
	char buffer[10];
	size_t pos = 9;
	while(value)
	{
		buffer[pos--] = value % 10 + '0';
		value /= 10;
	}
	return object_write(obj, buffer + pos + 1, 9 - pos);
}

ssize_t object_read(ssize_t obj, char *buffer, size_t length)
{
	if(obj < capacity && objects[obj].valid)
	{
		if(objects[obj].vtable->read)
			return objects[obj].vtable->read(objects[obj].state, buffer, length);
		else
			return ERR_NO_CALL;
	}
	else
		return ERR_NONE;
}

ssize_t object_set_pos(ssize_t obj, off64_t position)
{
	if(obj < capacity && objects[obj].valid)
	{
		if(objects[obj].vtable->set_pos)
			return objects[obj].vtable->set_pos(objects[obj].state, position);
		else
			return ERR_NO_CALL;
	}
	else
		return ERR_NONE;
}
