/* format of information in the invalid section and page entries
 * [1:0] = 00	indicated invalid entry
 * [2]			Fill space
 * [3]			Use big pieces, like large pages or supersections
 * [31:4]		ignored
 */

#include "mem.h"
#include <stdint.h>

#define KERNEL_MEMORY_START 0x80000000

typedef enum AlignmentKind
{
	ALIGN_SMALL_PAGE,
	ALIGN_LARGE_PAGE,
	ALIGN_SECTION,
	ALIGN_SUPERSECTION
} AlignmentKind;

static size_t total_memory_kib = 0;
static size_t memory_in_use_kib = 0;
// address of first free area in physical memory, 1KiB granularity
static size_t first_free_phys_area = 0;
// address of first free area in virtual memory, 4KiB granularity
static size_t first_free_virt_area = 0;

static volatile size_t *get_or_create_page_table(size_t va);
// `start` must be aligned to 4KiB
static char is_virt_space_free(size_t start, size_t size);
static size_t find_virt_space(size_t size, size_t align);

void mem_init(size_t total_mem_kib)
{
	total_memory_kib = total_mem_kib;
	uint8_t *used_bitfield = (void*)(KERNEL_MEMORY_START + 16 * 1024);
	for(size_t kib = 0; kib < total_mem_kib; kib++)
	{
		char used = (used_bitfield[kib / 8] >> (kib % 8)) & 1;
		if(used)
			memory_in_use_kib++;
		else if(!first_free_phys_area)
			first_free_phys_area = KERNEL_MEMORY_START + kib * 1024;
	}
	for(size_t va = KERNEL_MEMORY_START; va; va += 4096)
	{
		char found_free_space = 0;
		size_t section_entry = ((size_t*)KERNEL_MEMORY_START)[va / 1024 / 1024];
		if((section_entry & 0b11) == 0 && !first_free_virt_area)
		{
			first_free_virt_area = va;
			break;
		}
		else if((section_entry & 0b11) == 0b01)	// page table
		{
			size_t *page_table = (void*)(section_entry & 0b11111111111111111111110000000000);
			for(size_t page = 0; page < 256; page++)
			{
				if((page_table[page] & 0b11) == 0)
				{
					first_free_virt_area = va + page * 4096;
					found_free_space = 1;
					break;
				}
			}
		}
		if(found_free_space)
			break;
	}
}

void *mem_alloc(size_t size, size_t alignment)
{
	size_t size_kib = size / 1024 + (size % 1024 ? 1 : 0);
	alignment = (alignment / 1024 + (alignment % 1024 ? 1 : 0)) * 1024;
	size_t candidate = first_free_phys_area;
	uint8_t *used_bitfield = (void*)(KERNEL_MEMORY_START + 16 * 1024);
	char skipped_free = 0;
	while(candidate < KERNEL_MEMORY_START + total_memory_kib * 1024)
	{
		char free = 1;
		char aligned = candidate % alignment == 0;
		for(size_t kib = 0; kib < size_kib; kib++)
		{
			size_t index = (candidate - KERNEL_MEMORY_START) / 1024 + kib;
			char used = (used_bitfield[index / 8] >> (index % 8)) & 1;
			if(used)
			{
				free = 0;
				break;
			}
			else
				skipped_free = 1;
		}
		if(free && aligned)
		{
			if(!skipped_free)
				first_free_phys_area = candidate + size_kib * 1024;
			for(size_t kib = 0; kib < size_kib; kib++)
			{
				size_t index = (candidate - KERNEL_MEMORY_START) / 1024 + kib;
				used_bitfield[index / 8] |= 1 << (index % 8);
			}
			memory_in_use_kib += size_kib;
			return (void*)candidate;
		}
		else
			candidate = candidate + alignment - candidate % alignment;
	}
	return 0;
}

void mem_free(void *space, size_t size)
{
	size_t size_kib = size / 1024 + (size % 1024 ? 1 : 0);
	uint8_t *used_bitfield = (void*)(KERNEL_MEMORY_START + 16 * 1024);
	for(size_t kib = (size_t)space / 4; kib < (size_t)space / 4 + size_kib; kib++)
		used_bitfield[kib / 8] &= ~(1 << (kib % 8));
	memory_in_use_kib -= size_kib;
}

void *mem_valloc_delayed(size_t size, MemAllocationBlocks blocks)
{
	size_t align = 4096;
	switch(blocks)
	{
	case MEM_ALLOC_SMALL_PAGES:
		align = 4096;
		break;

	case MEM_ALLOC_LARGE_PAGES:
		align = 64 * 1024;
		break;

	case MEM_ALLOC_SECTIONS:
		align = 1024 * 1024;
		break;

	case MEM_ALLOC_SUPERSECTIONS:
		align = 16 * 1024 * 1024;
		break;
	}

	size_t candidate = find_virt_space(size, align);
	if(!candidate)
		return 0;

	switch(blocks)
	{
	case MEM_ALLOC_SMALL_PAGES:
	case MEM_ALLOC_LARGE_PAGES:
		(void)0;
		size_t num_pages = size / 4096 + (size % 4096 ? 1 : 0);
		for(size_t page = 0; page < num_pages; page++)
		{
			size_t table_index = (candidate + page * 4096) % (1024 * 1024) / 4096;
			get_or_create_page_table(candidate + page * 4096)[table_index]
				= (blocks == MEM_ALLOC_LARGE_PAGES) << 3 | 1 << 2;
		}
		break;

	case MEM_ALLOC_SECTIONS:
	case MEM_ALLOC_SUPERSECTIONS:
		(void)0;
		size_t num_sections = size / (1024 * 1024) + (size % (1024 * 1024) ? 1 : 0);
		for(size_t section = 0; section < num_sections; section++)
		{
			size_t table1_index = (candidate + section * 1024 * 1024) / (1024 * 1024);
			volatile size_t *table1 = (void*)KERNEL_MEMORY_START;
			table1[table1_index] = (blocks == MEM_ALLOC_SUPERSECTIONS) << 3 | 1 << 2;
		}
		break;
	}

	return (void*)candidate;
}

void *mem_map_device(size_t phys_addr, size_t size)
{
	size_t virt_start = find_virt_space(size, 4096);
	if(!virt_start)
		return 0;

	AlignmentKind align_kind = ALIGN_SMALL_PAGE;
	if(size >= 16 * 1024 * 1024 && virt_start % (16 * 1024 * 1024) == 0)
		align_kind = ALIGN_SUPERSECTION;
	else if(size >= 1024 * 1024 && virt_start % (1024 * 1024) == 0)
		align_kind = ALIGN_SECTION;
	else if(size >= 64 * 1024 && virt_start % (64 * 1024) == 0)
		align_kind = ALIGN_LARGE_PAGE;

	size_t num_supersections = align_kind == ALIGN_SUPERSECTION ? size / (16 * 1024 * 1024) : 0;

	size_t num_sections = align_kind == ALIGN_SECTION ? size / (1024 * 1024) :
		(align_kind == ALIGN_SUPERSECTION ? size % (16 * 1024 * 1024) / (1 * 1024 * 1024) : 0);

	size_t num_large_pages = align_kind == ALIGN_LARGE_PAGE ? size / (64 * 1024) :
		(align_kind == ALIGN_SECTION || align_kind == ALIGN_SUPERSECTION ?
			size % (1024 * 1024) / (64 * 1024) : 0);

	size_t num_small_pages = (align_kind == ALIGN_SMALL_PAGE ?
			size / 4096 : size % (64 * 1024) / 4096)
				+ (size % 4096 ? 1 : 0);

	size_t virt_pos = virt_start;
	size_t *table1 = (void*)KERNEL_MEMORY_START;

	// TEX = 000
	// C = 0
	// B = 0
	// S = 0
	// AP = 001
	for(size_t supersection = 0; supersection < num_supersections; supersection++)
	{
		size_t table_index = virt_pos / (1024 * 1024);
		for(size_t i = 0; i < 16; i++)
			table1[table_index + 1] =
				0	// PXN
				| 1 << 1	// supersection
				| 0 << 2	// B
				| 0 << 3	// C
				| 1 << 4	// XN
				| 1 << 10	// AP [1:0] = 01
				| 0 << 12	// TEX
				| 0 << 15	// AP[2]
				| 0 << 16	// S
				| 0 << 17	// nG
				| 1 << 18	// supersection
				| phys_addr;
		virt_pos += 16 * 1024 * 1024;
		phys_addr += 16 * 1024 * 1024;
	}

	for(size_t section = 0; section < num_sections; section++)
	{
		size_t table_index = virt_pos / (1024 * 1024);
		table1[table_index] =
			0	// PXN
			| 1 << 1	// section
			| 0 << 2	// B
			| 0 << 3	// C
			| 1 << 4	// XN
			| 1 << 10	// AP[1:0] = 01
			| 0 << 12	// TEX
			| 0 << 15	// AP[2]
			| 0 << 16	// S
			| 0 << 17	// nG
			| phys_addr;
		virt_pos += 1024 * 1024;
		phys_addr += 1024 * 1024;
	}

	for(size_t page = 0; page < num_large_pages; page++)
	{
		size_t table1_index = virt_pos / (1024 * 1024);
		size_t *table2 = 0;
		if((table1[table1_index] & 0b11) == 0b01)
			table2 = (size_t*)(table1[table1_index] & 0xfffffc00);
		else
		{
			table2 = mem_alloc(1024, 1024);
			if(!table2)
				return 0;
			table1[table1_index] =
				0b01	// page table
				| 0 << 2	// PXN
				| (size_t)table2;
		}
		size_t table2_index = virt_pos % (1024 * 1024) / 4096;
		for(size_t i = 0; i < 16; i++)
			table2[table2_index + i] =
				0b01	// large page
				| 0 << 2	// B
				| 0 << 1	// C
				| 1 << 4	// AP[1:0] = 01
				| 0 << 9	// AP[2]
				| 0 << 10	// S
				| 0 << 11	// nG
				| 0 << 12	// TEX
				| 1 << 15	// XN
				| phys_addr;
		virt_pos += 64 * 1024;
		phys_addr += 64 * 1024;
	}

	for(size_t page = 0; page < num_small_pages; page++)
	{
		size_t table1_index = virt_pos / (1024 * 1024);
		size_t *table2 = 0;
		if((table1[table1_index] & 0b11) == 0b01)
			table2 = (size_t*)(table1[table1_index] & 0xfffffc00);
		else
		{
			table2 = (size_t*)mem_alloc(1024, 1024);
			if(!table2)
				return 0;
			table1[table1_index] =
				0b01	// page table
				| 0 << 2	// PXN
				| (size_t)table2;
		}
		size_t table2_index = virt_pos % (1024 * 1024) / 4096;
		table2[table2_index] =
			1	// XN
			| 1 << 1	// small page
			| 0 << 2	// B
			| 0 << 3	// C
			| 1 << 4	// AP[1:0]
			| 0 << 6	// TEX
			| 0 << 9	// AP[2]
			| 0 << 10	// S
			| 0 << 11	// nG
			| phys_addr;
		virt_pos += 4096;
		phys_addr += 4096;
	}

	asm("dsb");
	// invalidate entire TLB
	asm("mcr p15, 0, %0, c8, c7, 0"
			:
			: "r" (1)
		);
	asm("dsb");
	asm("isb");

	return (void*)virt_start;
}

// TODO deallocate physical memory
void mem_unmap(void *virt_addr_, size_t size)
{
	size_t *table1 = (void*)KERNEL_MEMORY_START;
	size_t virt_start = (size_t)virt_addr_;
	if(virt_start < first_free_virt_area)
		first_free_virt_area = virt_start & ~4095;
	size_t num_pages = size / 4096 + (size % 4096 ? 1 : 0);
	for(size_t i = 0; i < num_pages; i++)
	{
		size_t section_idx = (virt_start + i * 4096) / (1024 * 1024);
		size_t section_status = table1[section_idx];
		if((section_status & 0b11) == 0b01)	// section table
		{
			size_t *table2 = (void*)(section_status & 0b11111111111111111111110000000000);
			size_t page_idx = (virt_start + i * 4096) % (1024 * 1024) / 4096;
			table2[page_idx] = 0;
		}
		else
			table1[section_idx] = 0;
	}
	asm("dsb");
	asm("mcr p15, 0, %0, c8, c7, 0" : : "r" (1));
	asm("dsb");
	asm("isb");
}

void mem_map_process_memmap(volatile size_t *map)
{
	asm("mcr p15, 0, %0, c2, c0, 1" : : "r" (map));
}

void *memcpy(void *restrict _dest, const void *restrict _src, size_t count)
{
	if((size_t)_dest % 4 == 0 && (size_t)_src % 4 == 0 && count % 4 == 0)
	{
		uint32_t *dest = _dest;
		const uint32_t *src = _src;
		for(size_t pos = 0; pos < count / 4; pos++)
			dest[pos] = src[pos];
	}
	else
	{
		uint8_t *dest = _dest;
		const uint8_t *src = _src;
		for(size_t pos = 0; pos < count; pos++)
			dest[pos] = src[pos];
	}
	return _dest;
}

void *memset(void *dest_, int ch, size_t count)
{
	if((size_t)dest_ % 4 == 0 && count % 4 == 0)
	{
		uint32_t val = ch << 24 | ch << 16 | ch << 8 || ch;
		uint32_t *dest = dest_;
		for(size_t i = 0; i < count / 4; i++)
			dest[i] = val;
	}
	else
	{
		uint8_t *dest = dest_;
		for(size_t i = 0; i < count; i++)
			dest[i] = ch;
	}
	return dest_;
}

size_t mem_usage(void)
{
	return memory_in_use_kib;
}

int mem_check_delayed_map_on_abort(void)
{
	size_t *table1 = (void*)KERNEL_MEMORY_START;
	size_t va;
	asm("mrc p15, 0, %0, c6, c0, 0" : "=r" (va));
	if(va < KERNEL_MEMORY_START)
		return 0;
	size_t section = va / (1024 * 1024);
	size_t section_status = table1[section];
	if((section_status & 0b11) == 0b01)	// page table
	{
		size_t *table2 = (void*)(section_status & 0b11111111111111111111110000000000);
		size_t page = va % (1024 * 1024) / 4096;
		size_t page_status = table2[page];
		if((page_status & 0b111) == 0b100)	// must map
		{
			if(page_status & 1 << 3)	// large pages
			{
				size_t first_page = page - page % 16;
				size_t target_pa = (size_t)mem_alloc(64 * 1024, 64 * 1024);
				if(!target_pa)
					return 0;
				for(size_t page_idx = 0; page_idx < 16; page_idx++)
					table2[first_page + page_idx] =
						1
						| 0 << 2	// B
						| 1 << 3	// C
						| 1 << 4	// AP[1:0]
						| 0 << 9	// AP[2] (0 = writable)
						| 0 << 10	// S
						| 0 << 12	// TEX
						| 1 << 15	// XN
						| target_pa;
				return 1;
			}
			else	// small page
			{
				size_t target_pa = (size_t)mem_alloc(4096, 4096);
				if(!target_pa)
					return 0;
				table2[page] =
					1	// XN
					| 1 << 1
					| 0 << 2	// B
					| 1 << 3	// C
					| 1 << 4	// AP[1:0]
					| 0 << 6	// TEX
					| 0 << 9	// AP[2]
					| target_pa;
				return 1;
			}
		}
		else
			return 0;
	}
	else if((section_status & 0b111) == 0b100)	// must map sections
	{
		if(section_status & 1 << 3)	// use supersections
		{
			size_t start = section - section % 16;
			size_t target_pa = (size_t)mem_alloc(16 * 1024 * 1024, 16 * 1024 * 1024);
			if(!target_pa)
				return 0;
			for(size_t i = 0; i < 16; i++)
				table1[start + i] =
					1 << 1
					| 0 << 2	// B
					| 1 << 3	// C
					| 1 << 4	// XN
					| 1 << 10	// AP[1:0]
					| 0 << 12	// TEX
					| 0 << 15	// AP[2]
					| 0 << 16	// S
					| 1 << 18
					| target_pa;
			return 1;
		}
		else	// sections
		{
			size_t target_pa = (size_t)mem_alloc(1024 * 1024, 1024 * 1024);
			if(!target_pa)
				return 0;
			table1[section] =
				1 << 1
				| 0 << 2	// B
				| 1 << 3	// C
				| 1 << 4	// XN
				| 1 << 10	// AP[1:0]
				| 0 << 12	// TEX
				| 0 << 15	// AP[2]
				| 0 << 16	// S
				| target_pa;
			return 1;
		}
	}
	else
		return 0;
}

static volatile size_t *get_or_create_page_table(size_t va)
{
	size_t *table1 = (void*)KERNEL_MEMORY_START;
	size_t table1_index = va / (1024 * 1024);
	size_t table2_status = table1[table1_index];
	if((table2_status & 0b11) == 1)	// page table
		return (void*)(table2_status & 0b11111111111111111111110000000000);
	else if((table2_status & 0b11) == 0)	// empty
	{
		size_t *table2 = mem_alloc(1024, 1024);
		for(size_t i = 0; i < 256; i++)
			table2[i] = 0;
		table1[table1_index] = (size_t)table2 | 1;
		return table2;
	}
	else
		return 0;
}

static char is_virt_space_free(size_t start, size_t size)
{
	size_t *table1 = (void*)KERNEL_MEMORY_START;
	size_t num_pages = size / 4096 + (size % 4096 ? 1 : 0);
	for(size_t page = 0; page < num_pages; page++)
	{
		size_t check_addr = start + page * 4096;
		size_t table1_index = check_addr / (1024 * 1024);
		size_t table2_status = table1[table1_index];
		if((table2_status & 2) == 2 || (table2_status & 1 << 2))
			// section, supersection or delayed
			return 0;
		else if(table2_status & 1)	// page table
		{
			size_t *table2 = (void*)(table2_status & 0b11111111111111111111110000000000);
			size_t table2_index = check_addr % (1024 * 1024) / 4096;
			if(table2[table2_index] & 0b111)
				return 0;
		}
	}
	return 1;
}

static size_t find_virt_space(size_t size, size_t align)
{
	size_t candidate = first_free_virt_area;
	char skipped_free = 0;
	while(1)
	{
		if(candidate % align == 0)
		{
			if(is_virt_space_free(candidate, size))
			{
				if(!skipped_free)
					first_free_virt_area = candidate + size;
				return candidate;
			}
			else if(is_virt_space_free(candidate, 4096))
				skipped_free = 1;
			candidate += align;
		}
		else
			candidate += align - candidate % align;
		if(!candidate)	// rolled over
			return 0;
	}
}
