#pragma once
#include <aleph.h>
#include "str.h"

typedef enum PrcmSubmodule
{
	PRCM_CM_PER,
	PRCM_CM_WKUP
} PrcmSubmodule;

typedef enum PrcmState
{
	PRCM_DISABLED = 0,
	PRCM_ENABLED = 0b10
} PrcmState;

typedef enum PrcmStrSubmoduleStatus
{
	PRCM_STR_SUBMODULE_OK,
	PRCM_STR_SUBMODULE_NO_MODULE
} PrcmStrSubmoduleStatus;

ssize_t prcm_init(void);
void prcm_switch(PrcmSubmodule submodule, size_t position, PrcmState state);
PrcmStrSubmoduleStatus prcm_switch_str_submodule(Str submodule, size_t position, PrcmState state);
