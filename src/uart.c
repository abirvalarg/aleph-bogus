// UART driver module

#include "uart.h"
#include <aleph.h>
#include <stdint.h>
#include "str.h"
#include "strmap.h"
#include "device.h"
#include "object.h"
#include "heap.h"
#include "mem.h"
#include "intc.h"
#include "process.h"

#define THR 0
#define RHR 0
#define IER (0x4 / 4)
#define IIR (0x8 / 4)
#define LSR (0x14 / 4)
#define SSR (0x44 / 4)

typedef struct TransferBufferChainNode
{
	uint8_t *buffer;
	size_t transferred;
	size_t length;
	struct TransferBufferChainNode *next;
} TransferBufferChainNode;

typedef struct State
{
	volatile size_t *regs;
	TransferBufferChainNode *transfer_chain_head;
	TransferBufferChainNode **transfer_chain_tail_slot;
	char *read_buffer;
	size_t read_buffer_start;
	size_t read_buffer_length;
	size_t read_buffer_size;
	uint8_t in_use: 1;
} State;

// methods
static void dev_cleanup(void *state);
static ssize_t create_device(Str name, const StrMap *options);
static ssize_t open(void *state, size_t flags);
static void obj_cleanup(void *state);
static ssize_t write(void *state, const char *buffer, size_t length);
static ssize_t read(void *state, char *buffer, size_t length);
static void on_irq(size_t int_num, void *state);

// helpers
static char is_buffer_full(State *state);
static void write_buffers(State *state);
static void read_bytes(State *state);

const Driver uart_driver = {
	.name = "uart",
	.create_device = create_device
};

static const DeviceVTable dev_vt = {
	.cleanup = dev_cleanup,
	.open = open
};

static const ObjectVTable obj_vt = {
	.cleanup = obj_cleanup,
	.write = write,
	.read = read
};

// TODO add options
static ssize_t create_device(Str name, const StrMap *options)
{
	Str *address_string = strmap_get(options, str_from_cstr("address"));
	if(!address_string)
		return ERR_OPTIONS;
	StrToU32Result address_res = str_hex_to_u32(*address_string);
	if(!address_res.success)
		return ERR_OPTIONS;
	size_t address = address_res.result;

	Str *int_num_string = strmap_get(options, str_from_cstr("int"));
	if(!int_num_string)
		return ERR_OPTIONS;
	StrToU32Result int_num_res = str_hex_to_u32(*int_num_string);
	if(!int_num_res.success)
		return ERR_OPTIONS;
	size_t int_num = int_num_res.result;

	Str *read_buffer_size_string = strmap_get(options, str_from_cstr("buffer:read:size"));
	size_t read_buffer_size = 4096;
	if(read_buffer_size_string)
	{
		StrToU32Result size_res = str_hex_to_u32(*read_buffer_size_string);
		if(!size_res.success)
			return ERR_OPTIONS;
		read_buffer_size = size_res.result;
	}

	State *state = heap_alloc(sizeof(State));
	if(!state)
		return ERR_OUT_OF_MEMORY;
	char *read_buffer = mem_alloc(read_buffer_size, 1);
	if(!read_buffer)
	{
		heap_free(state);
		return ERR_OUT_OF_MEMORY;
	}
	*state = (State) {
		.regs = mem_map_device(address, 4096),
		.read_buffer = read_buffer,
		.read_buffer_size = read_buffer_size
	};
	if(!state->regs)
	{
		heap_free(state);
		return ERR_OUT_OF_MEMORY;
	}
	state->transfer_chain_tail_slot = &state->transfer_chain_head;
	ssize_t st = device_register(name, &dev_vt, state);
	if(st != OK)
	{
		mem_unmap((void*)state->regs, 4096);
		heap_free(state);
		return st;
	}
	IntcHandler handler = {
		.handler = on_irq,
		.arg = state
	};
	intc_set_handler(int_num, handler);
	state->regs[IER] |= 1;
	return st;
}

static void dev_cleanup(void *state_)
{
	State *state = state_;
	while(state->transfer_chain_head);	// wait for all bytes to be transferred
	state->regs[IER] = 0;
	mem_unmap((void*)state->regs, 4096);
	mem_free(state->read_buffer, state->read_buffer_size);
	heap_free(state);
}

static ssize_t open(void *state_, size_t flags)
{
	(void)flags;
	State *state = state_;
	if(state->in_use)
		return ERR_BUSY;
	state->in_use = 1;
	return object_register(&obj_vt, state);
}

static void obj_cleanup(void *state_)
{
	State *state = state_;
	state->in_use = 0;
}

static ssize_t write(void *state_, const char *buffer, size_t length)
{
	State *state = state_;
	uint8_t *tr_buffer = heap_alloc(length);
	if(!tr_buffer)
		return ERR_OUT_OF_MEMORY;

	TransferBufferChainNode *node = heap_alloc(sizeof(TransferBufferChainNode));
	if(!node)
	{
		heap_free(tr_buffer);
		return ERR_OUT_OF_MEMORY;
	}

	memcpy(tr_buffer, buffer, length);
	*node = (TransferBufferChainNode) {
		.buffer = tr_buffer,
		.transferred = 0,
		.length = length,
		.next = 0
	};
	*state->transfer_chain_tail_slot = node;
	state->transfer_chain_tail_slot = &node->next;

	state->regs[IER] |= 0b10;

	return length;
}

static ssize_t read(void *state_, char *buffer, size_t length)
{
	State *state = state_;
	while(state->read_buffer_length == 0)
		process_pause();
	size_t read_len = length < state->read_buffer_length ? length : state->read_buffer_length;
	for(size_t pos = 0; pos < read_len; pos++, state->read_buffer_length--, state->read_buffer_start = (state->read_buffer_start + 1) % state->read_buffer_size)
		buffer[pos] = state->read_buffer[state->read_buffer_start];
	return read_len;
}

static void on_irq(size_t int_num, void *state_)
{
	(void)int_num;
	State *state = state_;
	size_t info = state->regs[IIR] >> 1 & 0b11111;
	if(info == 1)
		write_buffers(state);
	else if(info == 2)
		read_bytes(state);
}


static void write_buffers(State *state)
{
	TransferBufferChainNode *node = state->transfer_chain_head;
	if(!node)
	{
		state->regs[IER] &= ~0b10;
		return;
	}
	while(!is_buffer_full(state))
	{
		char buffer_full;
		for(; node->transferred < node->length && !(buffer_full = is_buffer_full(state)); node->transferred++)
			state->regs[THR] = node->buffer[node->transferred];
		if(node->transferred == node->length)
		{
			TransferBufferChainNode *next = node->next;
			heap_free(node->buffer);
			heap_free(node);
			state->transfer_chain_head = next;
			if(!next)
				state->transfer_chain_tail_slot = &state->transfer_chain_head;
		}
	}
}

static void read_bytes(State *state)
{
	while(state->regs[LSR] & 1)
	{
		char data = state->regs[RHR];
		if(state->read_buffer_length < state->read_buffer_size)
		{
			size_t pos = (state->read_buffer_start + state->read_buffer_length) % state->read_buffer_size;
			state->read_buffer[pos] = data;
			state->read_buffer_length++;
		}
	}
}

static char is_buffer_full(State *state)
{
	return state->regs[SSR] & 1;
}
