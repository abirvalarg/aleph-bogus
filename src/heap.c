#include "heap.h"
#include "mem.h"
#include "mutex.h"

#define FREE 0
#define TAKEN 1
#define LAST 2
#define STATUS_MASK 3

static size_t *heap_start = 0;
static size_t *first_free = 0;
// __attribute__((section(".bss.heap.mutex")))
static Mutex mutex = MUTEX_NEW;

/* Headers formats:
 * 	free block:
 * 		[31:2] = length / 4
 * 		[1:0] = "00"
 * 	taken block:
 * 		[31:2] = length / 4
 * 		[1:0] = "01"
 * 	last:
 * 		[31:2] = <don't care>
 * 		[1:0] = "10"
 */

#pragma GCC diagnostic ignored "-Warray-bounds"
void heap_init(size_t size)
{
	size_t num_words = size / 4 - 2;
	first_free = heap_start = mem_valloc_delayed(size, MEM_ALLOC_SECTIONS);
	*heap_start = num_words << 2 | FREE;
	size_t *last = heap_start + size / 4 - 1;
	*last = LAST;
}

void *heap_alloc(size_t size)
{
	mutex_lock(&mutex);
	size_t num_words = size / 4 + (size % 4 ? 1 : 0);
	if(!num_words)
		num_words = 1;

	size_t *block = first_free;
	char skipped_free = 0;
	while((*block & STATUS_MASK) != LAST)
	{
		size_t length = *block >> 2;
		size_t status = *block & STATUS_MASK;
		size_t *next = block + length + 1;
		if(status == FREE && length >= size)
		{
			next = block + size + 1;
			if(size < length)
				*next = (length - size - 1) << 2;
			if(!skipped_free)
				first_free = next;
			*block = size << 2 | TAKEN;
			void *result = block + 1;
			mutex_unlock(&mutex);
			return result;
		}
		else if(status == FREE && (*next & STATUS_MASK) == FREE)	// next block is free, concatenating
		{
			size_t new_length = length + (*next >> 2) + 1;
			*block = new_length << 2 | FREE;
			continue;
		}
		else if(status == FREE)
			skipped_free = 1;
		block = next;
	}
	mutex_unlock(&mutex);
	return 0;
}

void heap_free(void *_ptr)
{
	mutex_lock(&mutex);
	size_t *ptr = _ptr;
	size_t *header = ptr - 1;
	*header = (*header & ~STATUS_MASK) | FREE;
	size_t *next = header + (*header >> 2) + 1;
	if((*next & STATUS_MASK) == FREE)
		*header = ((*header >> 2) + (*next >> 2) + 1) << 2;
	if(header < first_free)
		first_free = header;
	mutex_unlock(&mutex);
}
