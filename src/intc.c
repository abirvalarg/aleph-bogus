#include "intc.h"
#include <aleph.h>
#include "mem.h"
#include <stddef.h>

#define SYSCONFIG (0x10 / 4)
#define SYSSTATUS (0x14 / 4)
#define SIR_IRQ (0x40 / 4)
#define CONTROL (0x48 / 4)
#define MIR_CLEAR0 (0x88 / 4)
#define MIR_CLEAR1 (0xa8 / 4)
#define MIR_CLEAR2 (0xc8 / 4)
#define MIR_CLEAR3 (0xe8 / 4)

static const size_t clear_regs[4] = { MIR_CLEAR0, MIR_CLEAR1, MIR_CLEAR2, MIR_CLEAR3 };
static volatile size_t *regs = 0;
static IntcHandler handlers[128] = {0};

ssize_t intc_init(void)
{
	regs = mem_map_device(0x48200000, 4096);
	if(!regs)
		return ERR_OUT_OF_MEMORY;

	// reset
	regs[SYSCONFIG] = 0b10;
	while(!(regs[SYSSTATUS] & 1));
	return OK;
}

IntcHandler intc_set_handler(size_t int_num, IntcHandler new_handler)
{
	if(int_num < 128)
	{
		IntcHandler old = handlers[int_num];
		handlers[int_num] = new_handler;
		regs[clear_regs[int_num / 32]] = 1 << (int_num % 32);
		return old;
	}
	else
		return (IntcHandler) { 0 };
}

void intc_on_irq(void)
{
	size_t int_num = regs[SIR_IRQ];
	if(handlers[int_num].handler)
		handlers[int_num].handler(int_num, handlers[int_num].arg);
	regs[CONTROL] = 1;	// new IRQ
}
