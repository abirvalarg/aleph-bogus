#include "device.h"
#include <aleph.h>
#include "strmap.h"
#include "heap.h"

typedef struct Device
{
	void *state;
	const DeviceVTable *vtable;
} Device;

static StrMap devices;

void device_init(void)
{
	devices = strmap_empty();
}

void device_cleanup(void)
{
	StrMapIter iter = strmap_iter(&devices);
	while(1)
	{
		StrMapIterItem item = strmap_iter_next(&iter);
		if(item.status == STRMAP_END_OF_ITER)
			break;
		Device *dev = item.item;
		if(dev->vtable->cleanup)
			dev->vtable->cleanup(dev->state);
		heap_free(dev);
	}
	strmap_del(&devices);
}

ssize_t device_register(Str name, const DeviceVTable *vtable, void *state)
{
	void *existing = strmap_get(&devices, name);
	if(existing)
		return ERR_BUSY;
	Device *dev = heap_alloc(sizeof(Device));
	if(!dev)
		return ERR_OUT_OF_MEMORY;
	*dev = (Device) {
		.state = state,
		.vtable = vtable
	};
	StrMapStatus st = strmap_insert(&devices, name, dev);
	if(st == STRMAP_OUT_OF_MEMORY)
	{
		heap_free(dev);
		return ERR_OUT_OF_MEMORY;
	}
	return OK;
}

ssize_t device_remove(Str name)
{
	Device *dev = strmap_get(&devices, name);
	if(dev)
	{
		if(dev->vtable->cleanup)
			dev->vtable->cleanup(dev->state);
		heap_free(dev);
		return OK;
	}
	else
		return ERR_NONE;
}

ssize_t device_open(Str name, size_t flags)
{
	Device *dev = strmap_get(&devices, name);
	if(!dev)
		return ERR_NONE;
	if(!dev->vtable->open)
		return ERR_NO_CALL;
	return dev->vtable->open(dev->state, flags);
}

ssize_t device_create_subdevice(Str parent_name, Str child_name, const StrMap *options)
{
	Device *parent = strmap_get(&devices, parent_name);
	if(!parent)
		return ERR_NONE;
	if(!parent->vtable->create_subdevice)
		return ERR_NO_CALL;
	if(strmap_get(&devices, child_name))
		return ERR_BUSY;
	return parent->vtable->create_subdevice(parent->state, child_name, options);
}
