#pragma once
#include <stddef.h>
#include <stdint.h>

typedef long ssize_t;
_Static_assert(sizeof(ssize_t) == sizeof(size_t));
typedef size_t off_t;
typedef int64_t off64_t;

// may be changed to a structure later with support for threads
typedef ssize_t PID;
typedef struct ThreadID
{
	PID process;
	ssize_t thread;
} ThreadID;

// No errors
#define OK 0
// No call is assiciated with the action
#define ERR_NO_CALL (-1)
// Not enough physical or virtual memory
#define ERR_OUT_OF_MEMORY (-2)
// Resource is already in use
#define ERR_BUSY (-3)
// No such object
#define ERR_NONE (-4)
// Action is not implemented yet
#define ERR_NOT_IMPLEMENTED (-5)
// Bad set of options
#define ERR_OPTIONS (-6)
// Attempt to execute action that's not available in current mode
#define ERR_MODE_MISMATCH (-7)
// The object is gone and should be closed
#define ERR_GONE (-8)

#define OPEN_READ 1
#define OPEN_WRITE 2
#define OPEN_APPEND 4

#define CALL_WRITE 0
#define CALL_READ 1
#define CALL_CLOSE 2
#define CALL_EXIT 3
#define CALL_OPEN_DEVICE 4
#define CALL_MEM_MAP 5
#define CALL_MEM_UNMAP 6

#define CUSTOM_SYSCALL_START (CALL_MEM_UNMAP + 1)
